#! /usr/bin/env python3

import random
import sys
import psycopg2

#Print menu function to print out the options
def print_menu():
	menu = ['1. Who fought the longest?', '2. Who fought the shortest?',
			'3. Who has the most possible attacks?', '4. Most fights won?',
			'5. Who lost the most fights?', '6. Wins/Loses/Draws by species.',
			'7. Quit?']
	
	for num in menu:
		print(num)

def main():

	if len(sys.argv) != 2:
		print("Incorrect syntax.")
		print("Usage: ./stats.py <db-name>")
		sys.exit()

	arg1 = sys.argv[1]

	try:
		con = psycopg2.connect(database=arg1)
	except Exception as e:
		print("Failed to connect to database")
		exit(1)

	cur = con.cursor()

	fighters = []

	#Main loop of program, keep printint out the menu and grab user input
	#then finally exiting the program once user input is 7
	while(1):
		print_menu()

		choice = input("Please enter your choice: ")
		
		#Query the fight table
		try:
			cur.execute('select * from fight')
		except Exception as e:
			print("Failed to access database, {0}".format(e))
			exit(1)

		#Grab everything from the table
		fight_results = cur.fetchall()

		#Query the combatant table
		try:
			cur.execute('select * from combatant')
		except Exception as e:
			print("Failed to access database, {0}".format(e))
			exit(1)

		#Grab everything from the table
		combatant_results = cur.fetchall()

		num = 0
		for item in combatant_results:
			if int(item[0]) > num:
				num = int(item[0])
		
		listTimeIn = [5]
		if choice < '3':
			#Query method derived from McLaurin
			sql_query = "SELECT id, name, SUM(finish-start)"
			sql_query += "FROM combatant JOIN fight ON id=combatant_one OR"
			sql_query += " id=combatant_two GROUP BY id ORDER BY sum DESC;"

			cur.execute(sql_query) 
			time = cur.fetchall()

			if choice == '1':
				print()
				print(time[0][1], "fought for", time[0][2])
				print()
			elif choice == '2':
				print()
				print(time[-1][1], "fought for", time[-1][2])
				print()
		#If choice is 3 then query the species_attack table and grab everything 
		#and then loop through each species to figure out who has the most attacks,
		#derived from Iracane
		elif choice == '3':
			most_attacks = [0,0]
			for result in combatant_results:
				try:
					cur.execute('select * from species_attack where species_id = ' + str(result[2]))
				except Exception as e:
					print("Failed to access database, {0}".format(e))
					exit(1)	
				
				num_attacks = cur.fetchall()
				
				if len(num_attacks) > most_attacks[1]:
					most_attacks[0] = result[0]
					most_attacks[1] = len(num_attacks)

			for result in combatant_results:
				if result[0] == most_attacks[0]:
					print()
					print(result[1], "has the most possible attacks numbering", most_attacks[1])
					print()
		#If choice is 4 or 5 then use  
		#and then loop through each species to figure out who has the most attacks,
		#derived from Iracane
		elif choice >= '4' and choice <= '5':
			listo = [2]
			for result in combatant_results:
				listo.append(0)
			
			if choice == '4':
				for x in range(1, len(combatant_results)):
					for something in fight_results:
						if ((int(something[0]) == x and something[2] == "One") or (int(something[1]) == x and something[2] == "Two")):
							listo[x]+=1		
				print()
				print("Most wins was: ", max(listo))
				print()
			elif choice == '5':
				for x in range(1, len(combatant_results)):
					for something in fight_results:
						if ((int(something[1]) == x and something[2] == "One") or (int(something[0]) == x and something[2] == "Two")):
							listo[x]+=1
				print()
				print("The most losses was: ", max(listo))
				print()
		#If choice is 6 then query the combatant table and grab everything, then go thorough and track every win, loss, and draw
		#eventually entering a loop where the stored information is printed out
		#derived from Primm
		elif choice == '6':
			try:
				cur.execute('select count(*) from combatant')
			except Exception as e:
				print("Failed to access database, {0}".format(e))
				exit(1)

			total = cur.fetchall()[0][0]
			fighters = []
			combatant_list = {}
			
			for i in range(1, total+1):
				cur.execute("select count(*) from fight where 'One' = winner and combatant_one = %s or 'Two' = winner and combatant_two = %s",( i, i))
				fighters.append([])
				fighters[i-1].append(i)
				fighters[i-1].append(cur.fetchall()[0][0])
				cur.execute("select count(*) from fight where 'Two' = winner and combatant_one = %s or 'One' = winner and combatant_two = %s",( i, i))
				fighters[i-1].append(cur.fetchall()[0][0])
				cur.execute("select count(*) from fight where 'Tie' = winner and combatant_one = %s or 'Tie' = winner and combatant_two = %s",( i, i))
				fighters[i-1].append(cur.fetchall()[0][0])
			for combatant in fighters:
				cur.execute('select species_id from combatant where id = %s', (combatant[0],))
				cur.execute('select name from species where id = %s', (cur.fetchall()[0][0],))
				ret_val = cur.fetchall()[0][0]
				if ret_val in combatant_list.keys():
					combatant_list[ret_val][0].append(combatant[0])
					combatant_list[ret_val][1] += combatant[1]
					combatant_list[ret_val][2] += combatant[2]
					combatant_list[ret_val][3] += combatant[3]
				else:
					combatant_list[ret_val] = [[combatant[0]], combatant[1], combatant[2], combatant[3]]
			
			win = 'wins'
			loss = 'losses'
			draw = 'draws'
	
			for i in combatant_list:
				if combatant_list[i][1] == 1:
					win = 'win'
				else:
					win = 'wins'
				
				if combatant_list[i][2] == 1:
					loss = 'loss'
				else:
					loss = 'losses'

				if combatant_list[i][3] == 1:
					draw = 'draw'
				else:
					draw = 'draws'

				print("%s\t\t  %d %s\t%d %s\t%d %s" %(i, combatant_list[i][1], win, combatant_list[i][2], loss, combatant_list[i][3], draw))
		#If choice is 7 then exit the program
		elif choice == '7':
			print("Exiting the program, thank you for your time.")
			sys.exit()
	
	con.close()				

if __name__ == "__main__":
	main()



























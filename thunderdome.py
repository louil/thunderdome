#! /usr/bin/env python3

import psycopg2
import random
import sys
import time
import itertools
import datetime

#Class for the fighters
class Character:
	def __init__(self, char_id, species_name, c_type, base_atk, base_dfn, base_hp):
		self.char_id = char_id
		self.species_name = species_name
		self.c_type = c_type
		self.base_atk = base_atk
		self.base_dfn = base_dfn
		self.base_hp = base_hp
		self.name = []
		self.attack = []

#Class for the attacks
class Attack:
	def __init__(self, atk_id, name, a_type, min_dmg, max_dmg, atk_speed):
		self.atk_id = atk_id
		self.name = name
		self.a_type = a_type
		self.min_dmg = min_dmg
		self.max_dmg = max_dmg
		self.atk_speed = atk_speed

#Derived from Primm
def dmg_calc(attacker, defender, attack, type_chart):
	try:
		dmg = type_chart[attacker.c_type][defender.c_type]
	except:
		dmg = 1

	try:
		atk = type_chart[attack.attack_type][defender.c_type]
	except:
		atk = 1
	
	if defender.base_dfn - attacker.base_atk < 1:
		dmg = 0 + (atk * random.randint(attack.min_dmg, attack.max_dmg))
	else:
		dmg = (dmg * random.randint(0, defender.base_dfn - attacker.base_atk)) + (atk * random.randint(attack.min_dmg, attack.max_dmg))

	return dmg

#Derived from Primm
def fight(fighter, defender, type_chart):
	fighter_hp = fighter.base_hp
	defender_hp = defender.base_hp
	fighter_wait = 0
	defender_wait = 0
	time_elapsed = 0

	while fighter_hp >= 0 and defender_hp >= 0:
		if fighter_wait == 0:
			fighter_attack = random.choice(fighter.attack)
			fighter_wait = fighter_attack.atk_speed.total_seconds()
		else:
			fighter_wait -= 1
			if fighter_wait == 0:
				dmg = dmg_calc(fighter, defender, fighter_attack, type_chart)
				defender_hp -= dmg
		
		if defender_wait == 0:
			defender_attack = random.choice(defender.attack)
			defender_wait = defender_attack.atk_speed.total_seconds()
		else:
			defender_wait -= 1
			if defender_wait == 0:
				dmg = dmg_calc(defender, fighter, defender_attack, type_chart)
				fighter_hp -= dmg
			
		time_elapsed += 1

	if fighter_hp >= 0 and defender_hp < 0:
		return 2,  time_elapsed#victory
	elif fighter_hp < 0 and defender_hp >= 0:
		return 1, time_elapsed #defeat
	else:
		return 0, time_elapsed #draw

# Found at http://stackoverflow.com/questions/4048651/python-function-to-convert-seconds-into-minutes-hours-and-days
def ddhhmmss(seconds):
	dhms = ''
	for scale in 86400, 3600, 60:
		result, seconds = divmod(seconds, scale)
		if dhms != '' or result > 0:
			dhms += '{0:02d}:'.format(result)

	dhms += '{0:02d}'.format(seconds)

	if len(dhms) < 3:
		dhms = '00:00:' + dhms
	elif len(dhms) <=5:
		dhms = '00:' + dhms
	return dhms

def main():
	fighters = []
	finished_combat = []
	attacks = []
	species_attacks = []
	type_chart = {"Physical":{"Biological":2, "Chemical":.5},
				"Biological":{"Radioactive":2, "Technological":2},
				"Radioactive":{"Physical":2, "Chemical":.5, "Technological":.5, "Mystical":2},
				"Chemical":{"Biological":2, "Radioactive":.5, "Chemical":.5, "Technological":.5},
				"Technological":{"Radioactive":.5, "Chemical":2},
				"Mystical":{"Physical":.5, "Biological":2, "Radioacitve":2, "Chemical":.5, "Technological":.5, "Mystical":.5},
				"Mineral":{"Mineral":0}
				}

	if len(sys.argv) != 2:
		print("Incorrect syntax.")
		print("Usage: ./thunderdome.py <db-name>")
		sys.exit()

	arg1 = sys.argv[1]

	try:
		con = psycopg2.connect(database=arg1)
	except Exception as e:
		print("Failed to connect to database")
		exit(1)

	cur = con.cursor()

	#Query the fight table
	try:
		cur.execute('select * from fight')
	except Exception as e:
		print("Failed to access database, {0}".format(e))	

	#Grab everything from the table
	x = cur.fetchall()

	#If what was grabbed is greater than 0 then the tournament has already been hosted and exit
	if len(x) > 0:
		print("Tournament has already been hosted.")
		con.commit()
		con.close()
		exit(1)

	#Query the species table
	try:
		cur.execute('select * from species')
	except Exception as e:
		print("Failed to access database, {0}".format(e))
		exit(1)

	#Grab everything from the table
	x = cur.fetchall()

	#Append to the fighters list a class object
	for i in x:
		fighters.append(Character(i[0], i[1], i[2], i[3], i[4], i[5]))

	try:
		cur.execute('select count(*) from combatant')
	except Exception as e:
		print("Failed to access database, {0}".format(e))
		exit(1)

	#Grab everything from the table
	num_of_combatants = cur.fetchall()

	#If the combatant table is empty then populate it with 14 names, 1 per species
	if num_of_combatants == 0:
		names = ['Aaron', 'Bob', 'Coxy', 'Daemon', 'Envy', 'Fox', 'Green', 'Hope', 'Icarus', 'Jones', 'Kinky', 'Leema', 'Moon', 'Pops']

		count = 0
		for i in fighters:
			cur.execute("insert into combatant (name, species_id, plus_atk, plus_dfn, plus_hp) values (%s, %s, %s, %s, %s);", (names[count], w.char_id, 0, 0, 0))
			fighter[i].name = names[count]	
			count+=1

	#Query the attack table
	try:
		cur.execute('select * from attack')
	except Exception as e:
		print("Failed to access database, {0}".format(e))
		exit(1)

	#Grab everything from the table
	x = cur.fetchall()

	#Add the attack class object into the attacks list
	for i in x:
		attacks.append(Attack(i[0], i[1], i[2], i[3], i[4], i[5]))

	#Query the species_attack table
	try:
		cur.execute('select * from species_attack')
	except Exception as e:
		print("Failed to access database, {0}".format(e))
		exit(1)

	#Grab everything from the table
	x = cur.fetchall()

	for i in x:
		fighters[i[0]-1].attack.append(attacks[i[1]-1])

	#Itertools usage derived from Primm
	one_pair = itertools.combinations(fighters, 2)

	#For loop that goes through and will insert into the fight table the appropriate information depending 
	#on if the combatants won, lost, or tiedS
	for pair in one_pair:
		outcome = fight(pair[0], pair[1], type_chart)
		
		end_time = 0

		start_time = '2016-06-10 ' + '00:00:00'

		end_time += outcome[1]

		end_time = '2016-06-10 ' + ddhhmmss(end_time)

		if outcome[0] == 0: #draw game
			cur.execute("insert into fight (combatant_one, combatant_two, winner, start, finish) values (%s, %s, 'Tie', TIMESTAMP %s, TIMESTAMP %s)", (pair[0].char_id, pair[1].char_id, start_time, end_time))
		elif outcome[0] == 1: #loss
			cur.execute("insert into fight (combatant_one, combatant_two, winner, start, finish) values (%s, %s, 'Two', TIMESTAMP %s, TIMESTAMP %s)", (pair[0].char_id, pair[1].char_id, start_time, end_time))
		else: #victory
			cur.execute("insert into fight (combatant_one, combatant_two, winner, start, finish) values (%s, %s, 'One', TIMESTAMP %s, TIMESTAMP %s)", (pair[0].char_id, pair[1].char_id, start_time, end_time))

	con.commit()
	con.close()

if __name__ == "__main__":
	main()

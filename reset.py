#! /usr/bin/env python3

import psycopg2
import random
import sys

#Connect and then delete the information in the fight table
def main():
	if len(sys.argv) != 2:
		print("Incorrect syntax.")
		print("Usage: ./reset.py <db-name>")
		sys.exit()

	arg1 = sys.argv[1]

	try:
		con = psycopg2.connect(database=arg1)
	except Exception as e:
		print("Failed to connect to database")
		exit(1)

	cur = con.cursor()

	try:
		cur.execute("delete from fight *")
	except Exception as e:
		print("Failed to connect to database, {0}".format(e))
		exit(1)

	con.commit()
	con.close()

if __name__ == "__main__":
	main()
